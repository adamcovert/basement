$(document).ready(function(){
  $('.contacts__slider').owlCarousel({
    items: 1,
    dots: false,
    margin: 5,
    center: true,
    loop: true,
    stagePadding: 75,
    nav: true,
    navText: ['<img src="img/arrow-left.png" alt="" />','<img src="img/arrow-right.png" alt="" />'],
  });

  $(function(){
    $('[data-toggle="tooltip"]').tooltip()
  })
});